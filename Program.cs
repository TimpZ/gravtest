﻿using System;

namespace ClassTesting
{



    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Press Enter");
            Console.ReadKey();

            //Spawn an instance of the Hero class
            PositionClass spawn = new PositionClass(0, 100, 0, false); //put him 100 m up in the air
            Hero hero1 = new Hero("Mr Hero", spawn);

            //Declare some universal variables
            float gravityAcceleration = -10f; // m/s
            float timeStep = 1f; // s


            //Create a run-loop
            bool running = true;
            while (running)
            {
                //Run the method that updates the position of the hero-instance
                hero1.UpdatePos(gravityAcceleration, timeStep);

                //Print the results of 
                Console.WriteLine("Current y position is " + hero1.Pos.Y + "m with dV being " + hero1.Pos.Velocity +"m/s");
                Console.ReadKey();
            }
        }
    }
}
        

        
    

