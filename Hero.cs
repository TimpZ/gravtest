﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassTesting
{
    class Hero
    {
        //variables
        public string Name { get; }
        public PositionClass Pos { get; set; }

        //constructors
        public Hero()
        {
            Name = "NoName";
            Pos = new PositionClass(0, 0, 0, false);
        }

        //overloaded constructors
        public Hero(string name, PositionClass pos)
        {
            //assign custom vars
            Name = name;
            Pos = pos;
            
        }


        //methods
        public void UpdatePos(float acc, float t)
        {
            Pos = Gravity.GravCalc(Pos, acc, t);
        }

    }
}
