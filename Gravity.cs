﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassTesting
{
    static class Gravity
    {
        public static PositionClass GravCalc(PositionClass pos, float acc, float t)
        {
            pos.Velocity += acc * t;
            pos = CheckIfGrounded(pos, acc, t);

            if (pos.Grounded)
            {
                pos.Velocity = 0;
            }

            pos.Y += pos.Velocity * t;
            return pos;
        }

        private static PositionClass CheckIfGrounded(PositionClass p, float acc, float t)
        {
            float nextPos = p.Y + (p.Velocity + acc * t)*t;

            if (nextPos <= 0)
            {
                p.Y = 0;
                p.Grounded = true;
            }
            else
            {
                p.Grounded = false;
            }

            return p;
        }
    }
}
