﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassTesting
{
    public class PositionClass
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Velocity { get; set; }
        public bool Grounded { get; set; }

        public PositionClass(float x, float y, float velocity, bool grounded)
        {
            X = x;
            Y = y;
            Velocity = velocity;
            Grounded = grounded;
        }
    }
}
